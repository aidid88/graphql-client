import { ApolloClient, InMemoryCache, gql } from '@apollo/client';
import LottieCard from '../../components/LottieCard';

export default function LottieIndex({ lottieFiles }) {
  console.log('lottie', lottieFiles);
  return (
    <div className="container md:flex mx-auto">
      <div>
        <h1 className="text-3xl py-10 primary-font-color">Gallery</h1>
      </div>
      <div className="md:flex mx-auto flex-wrap content-start">
        <LottieCard></LottieCard>
        <LottieCard></LottieCard>
        <LottieCard></LottieCard>
        <LottieCard></LottieCard>
        <LottieCard></LottieCard>
        <LottieCard></LottieCard>
        <LottieCard></LottieCard>
        <LottieCard></LottieCard>
      </div>
    </div>
  );
}

export async function getStaticProps() {
  const client = new ApolloClient({
    uri: 'http://localhost:3000/graphql',
    cache: new InMemoryCache(),
  });

  const { data } = await client.query({
    query: gql`
      query {
        lotties {
          id
          name
          description
          filePath
        }
      }
    `,
  });

  return {
    props: {
      lottieFiles: data.lotties,
    },
  };
}
